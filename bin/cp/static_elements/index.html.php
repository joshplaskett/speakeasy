<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Speak Easy Tech</title>
	<link rel="stylesheet" media="screen and (max-width:600px)" href="assets/style/mobile_style.css" type="text/css" />
	<link rel="stylesheet" media="screen and (min-width:600px)" href="assets/style/style.css" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Karla&display=swap" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>

<div class="content">
    
    

    <div class="main" id="main">
    
    <div id="home">
        <h1>Natural Customer Engagement. Delivered.</h1>
        
        <div class="splash_image">
        	<img src="/assets/images/Echo2.jpg">
            </div>
	<div class="button" id="enquireButton">Enquire</div>
        <p>We're Speak Easy Tech, proud developers of voice user interfaces that deliver customer-centric experiences.</p>
	<hr/>
	<h1>WHAT WE DO</h1>
	<div class="clearfix">
		<img src="/assets/images/Echo2.jpg" class="left small"/>
		<p>As the adoption of Smart Speakers and IoT devices continues to grow at a rapid pace, it is clear that voice interaction for inter-connectivity is here to stay. With a plethora of applications and solutions flooding the market, we at Speak Easy found that many offers failed to capture natural conversation to deliver engaging experiences for end users. With this in mind, we set out to revolutionise the market and bring intuitive, engaging and fun back to voice interaction.</p>
	</div>

	<div class="clearfix">
		<div class="right small" style="background:white;box-shadow:2px 2px 2px #333; border-radius:10px;">
			<h3 style="text-align:center;">Speak Easy Concierge</h3>
			<img src="assets/images/hotel.jpg" style="width:100%;"/>
		</div>
		<p>Empower your customers to leverage your expert hospitality with the power of their voice. </p>
	</div>
    </div>

    <div id="about">
        <h1>Who We Are</h1>
	<div class="button" id="enquireButton">Enquire</div>
        <p>We're Speak Easy Tech, proud developers of voice user interfaces that deliver customer-centric experiences.</p>
	<hr/>
	<h1>WHAT WE DO</h1>
        <p>As the adoption of Smart Speakers and IoT devices continues to grow at a rapid pace, it is clear that voice interaction for inter-connectivity is here to stay. With a plethora of applications and solutions flooding the market, we at Speak Easy found that many offers failed to capture natural conversation to deliver engaging experiences for end users. With this in mind, we set out to revolutionise the market and bring intuitive, engaging and fun back to voice.</p>
    </div>

    <div id="contact">
        <h1>Get in Touch</h1>
	<p>Got a great idea? Not sure how to maximise voice engagement? Or just fancy a chat? We would love to hear from you.</p>
	<form id="contactUsForm">
	  <label for="Name">Name</label>
	  <input id="Name" type="text"/><br/>
	  <label for="Email">Email</label>
	  <input id="Email" type="text"/><br/>
	  <label for="Message">Message</label>
	  <input id="Message" type="text" style="height:120px;border-radius:5px;"/><br/>
	  <input class="button" id="submit" type="submit"/>
	</form>
    </div>
    </div>
</div>

<script>



</script>
</body>
</html>